#!/bin/bash
ROOT=$(dirname "$(readlink "$BASH_SOURCE" || echo "$BASH_SOURCE")")
SRC="$ROOT/sources"
DEST="$ROOT/export"
BITRATE="1000k"
BITRATE_MAX="2000k"
WIDTH=640 # must be divisible by 8
FFMPEG="$ROOT/ffmpeg"

for file in "$SRC/"**.mov
do
	filename=`basename "$file"`

	echo "################################################"
	echo "# Encoding" ${filename%%.*}
	echo "################################################"

	# preview
	"$FFMPEG" -y -i "$file" -qscale:v 1 -q:v 1 -vframes 1 "$DEST/${filename%%.*}-preview.jpg"

	# x264
	"$FFMPEG" -y -i "$file" -an -codec:v libx264 -pix_fmt yuv420p -profile:v high -preset veryslow -level 4.1 -refs 4 -b:v $BITRATE -bt:v $BITRATE_MAX -qmin 0 -qmax 50 -pass 1 -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -f mp4 -threads 0 "/dev/null"
	"$FFMPEG" -y -i "$file" -an -codec:v libx264 -pix_fmt yuv420p -profile:v high -preset veryslow -level 4.1 -refs 4 -b:v $BITRATE -bt:v $BITRATE_MAX -qmin 0 -qmax 50 -pass 2 -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -f mp4 -threads 0 -strict -2 "$DEST/${filename%%.*}-small.mp4"

	# webm
	"$FFMPEG" -y -i "$file" -an -codec:v libvpx-vp9 -pix_fmt yuv420p -quality good -cpu-used 0 -qmin 0 -qmax 50 -crf 6 -b:v $BITRATE -maxrate $BITRATE_MAX -sws_flags bilinear -tile-columns 6 -frame-parallel 1 -pass 1 -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -f webm "/dev/null"
	"$FFMPEG" -y -i "$file" -an -codec:v libvpx-vp9 -pix_fmt yuv420p -quality good -cpu-used 0 -qmin 0 -qmax 50 -crf 6 -b:v $BITRATE -maxrate $BITRATE_MAX -sws_flags bilinear -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 -pass 2 -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -f webm "$DEST/${filename%%.*}-small.webm"

	# debug
	# "$FFMPEG" -y -i "$file" -t 2 -an -codec:v libx264 -pix_fmt yuv420p -profile:v high -preset veryslow -level 4.1 -refs 4 -b:v $BITRATE -bt:v $BITRATE_MAX -qmin 0 -qmax 50 -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -pass 1 -f mp4 -threads 0 "/dev/null"
	# "$FFMPEG" -y -i "$file" -t 2 -an -codec:v libx264 -pix_fmt yuv420p -profile:v high -preset veryslow -level 4.1 -refs 4 -b:v $BITRATE -bt:v $BITRATE_MAX -qmin 0 -qmax 50 -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -pass 2 -f mp4 -threads 0 -strict -2 "$DEST/${filename%%.*}-small.mp4"

	# "$FFMPEG" -y -i "$file" -t 2 -an -codec:v libvpx-vp9 -quality good -cpu-used 0 -qmin 0 -qmax 50 -crf 6 -b:v $BITRATE -maxrate $BITRATE_MAX -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -pix_fmt yuv420p -tile-columns 6 -frame-parallel 1 -pass 1 -f webm "/dev/null"
	# "$FFMPEG" -y -i "$file" -t 2 -an -codec:v libvpx-vp9 -quality good -cpu-used 0 -qmin 0 -qmax 50 -crf 6 -b:v $BITRATE -maxrate $BITRATE_MAX -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -pix_fmt yuv420p -tile-columns 6 -frame-parallel 1 -pass 2 -auto-alt-ref 1 -lag-in-frames 25 -f webm "$DEST/${filename%%.*}-small.webm"
	# "$FFMPEG" -y -i "$file" -t 2 -an -codec:v libvpx-vp9 -quality good -cpu-used 0 -qmin 0 -qmax 50 -crf 6 -b:v $BITRATE -maxrate $BITRATE_MAX -sws_flags bilinear -vf "scale=$WIDTH:trunc($WIDTH/a/2)*2" -pix_fmt yuv444p -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 -f webm "$DEST/${filename%%.*}-small.webm"
done